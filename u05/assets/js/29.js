

// try {
//   if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN')) {
//     setCookie("Intentos", "0", 1);
//   }
//   if (getCookie("Intentos") == "0") {}
// } catch (e) {
//
// }
//
// function getCookie(name) {
//   var start = document.cookie.indexOf(name + "=");
//   var len = start + name.length + 1;
//   if ((!start) && (name != document.cookie.substring(0, name.length))) {
//     return null;
//   }
//   if (start == -1) return null;
//   var end = document.cookie.indexOf(';', len);
//   if (end == -1) end = document.cookie.length;
//   return unescape(document.cookie.substring(len, end));
// }
//
// function setCookie(name, value, expires, path, domain, secure) {
//   var today = new Date();
//   today.setTime(today.getTime());
//   if (expires) {
//     expires = expires * 1000 * 60 * 60 * 24;
//   }
//   var expires_date = new Date(today.getTime() + (expires));
//   document.cookie = name + '=' + escape(value) +
//     ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
//     ((path) ? ';path=' + path : '') +
//     ((domain) ? ';domain=' + domain : '') +
//     ((secure) ? ';secure' : '');
// }
var intentosEjercicio = 0;
const franjaCorrecta = '<div id = "franjaVerde" class="lineaCorrecta" ></div>';
const franjaIncorrecta = '<div id = "franjaRoja" class="lineaIncorrecta" ></div>';



function quitarOverly() {

  var invisible = document.getElementsByClassName("overly")[0];
  var error = document.getElementById("final_error");
  var correcto = document.getElementById("final_ok");

  invisible.style.display = "none";
  error.style.display = "none";
  correcto.style.display = "none";
}


function evaluar() {

  var x = "";
  var valorCorrecto = 0;
  var incorrecto = 0;
  var mensajeRespuesta="Su respuesta es incorrecta";

  try {

    var respuesta1 = document.getElementById("RadioGroup1_0");
    var respuesta2 = document.getElementById("RadioGroup1_1");
    var respuesta3 = document.getElementById("RadioGroup1_2");

    // respuesta1

    if (respuesta1.checked) {
      valorCorrecto+=1;

    }
    // respuesta  2
    if (respuesta2.checked) {
      incorrecto+=1;
      mensajeRespuesta="Lamentablemente, su respuesta es incorrecta. El seguimiento y la evaluación no solo son necesarios al finalizar los programas. El seguimiento de una intervención y la evaluación de los impactos contribuyen a garantizar la adopción de medidas correctivas y preventivas y a comprender qué es lo que funciona, que es lo que no, y por qué.";
    }
    // respuesta  3
    if (respuesta3.checked) {
      mensajeRespuesta="Lamentablemente, su respuesta es incorrecta. Si  bien el fortalecimiento de la capacidad de las personas es importante, la sostenibilidad solo se puede alcanzar si se institucionalizan las cuestiones del trabajo infantil en los procedimientos operativos.";
      incorrecto+=1;

    }



    if ((valorCorrecto == 2) && (incorrecto==0)) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      botonesNavegacion();
      mensajeCorrecto('¡Su respuesta es correcta! No solo es necesario investigar sobre la situación del trabajo infantil, sino también analizar las alternativas viables, en particular las tecnologías que ahorran mano de obra en las tareas que habitualmente realizan los niños y que lkes impiden ir a la escuela, y las tecnologías y prácticas más seguras.', 'quitarOverly()', 'Continuar')
      // deleteCookie("Intentos");
    } else {
      // const intentosEjercicio = parseInt(getCookie("Intentos"));
      // var mensajeRespuesta = "Su respuesta es incorrecta";
      mostrarOverly();
      todosLosIntentos(mensajeRespuesta, '', '');
    }
  } catch (e) {
    console.log(e);
    quitarOverly();
  }

}

function todosLosIntentos(mensaje , funcion , mensajeBoton){
  document.getElementById('final_error').innerHTML='<div class="modal-dialog modal-confirm-error">' +
    '<div class="modal-content">' +
      '<div class="modal-header">' +
        '<div class="icon-box">' +
          '<i class="material-icons">&#xE5CD;</i>' +
        '</div>' +
        '<h4 class="modal-title" style="font-size:1em">'+ mensaje+ '</h4>' +
      '</div>' +
      '<div class="modal-footer">' +
        '<button type="button" class="btn btn-danger back" onclick="respuestaCorrecta()">Ver respuesta</button>' +
        '<button type="button" class="btn btn-danger back" onclick="reiniciarBotones()">Volver a intentar</button>' +
      '</div>' +
    '</div>' +
  '</div>';
}

function respuestaCorrecta() {
  quitarOverly();
  botonesNavegacion();

  document.getElementById("RadioGroup1_0").checked = true;
  document.getElementById("RadioGroup1_0").parentNode.style.backgroundColor="#39EF8F"

  document.getElementById("RadioGroup1_1").checked = false;

  document.getElementById("RadioGroup1_2").checked = false;







}
function reiniciarBotones() {
  quitarOverly();
  // document.getElementById("baseRespuestas").innerHTML = '';

  document.getElementById("RadioGroup1_0").checked=false;
  document.getElementById("RadioGroup1_1").checked=false;
  document.getElementById("RadioGroup1_2").checked=false;

  document.getElementById("RadioGroup1_0").parentNode.style.backgroundColor="";
}
