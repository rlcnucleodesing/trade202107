// Saved as file "SCORMObjectiveLogic.js"

function SCOSetObjectiveData(id, elem, v) {
	var result = "false";
  var i = SCOGetObjectiveIndex(id);
  if (isNaN(i)) {
    i = parseInt(SCOGetValue("cmi.objectives._count"));
    if (isNaN(i)) i = 0;
    if (SCOSetValue("cmi.objectives." + i + ".id", id) == "true"){
    	result = SCOSetValue("cmi.objectives." + i + "." + elem, v)
		}
  } else {
		result = SCOSetValue("cmi.objectives." + i + "." + elem, v);
		if (result != "true") { 
      // Maybe this LMS accepts only journaling entries
   		i = parseInt(SCOGetValue("cmi.objectives._count"));
			if (!isNaN(i)) {
				if (SCOSetValue("cmi.objectives." + i + ".id", id) == "true"){
		    	result = SCOSetValue("cmi.objectives." + i + "." + elem, v)
				}
			}
		}
  }
	return result
}

function SCOGetObjectiveData(id, elem) {
  var i = SCOGetObjectiveIndex(id);
  if (!isNaN(i)) {
    return SCOGetValue("cmi.objectives." + i + "."+elem)
  }
  return ""
}

function SCOGetObjectiveIndex(id){
  var i = -1;
  var nCount = parseInt(SCOGetValue("cmi.objectives._count"));
  if (!isNaN(nCount)) {
		for (i = nCount-1; i >= 0; i--){ //walk backward in case LMS does journaling
      if (SCOGetValue("cmi.objectives." + i + ".id") == id) {
        return i
      }
    }
  }
  return NaN
}

//alert("SCORMObjectiveLogic.js loaded")